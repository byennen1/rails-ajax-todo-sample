# frozen_string_literal: true

class SubtasksController < ApplicationController
  before_action :authenticate_user!
  respond_to :js
  responders :flash

  def create
    @task = Task.find(params[:task_id])
    @subtask = @task.subtasks.new(subtask_params)
    if @subtask.save
      respond_with(@subtask)
    else
      flash[:error] = "Can not be blank."
    end
  end

  private
    def subtask_params
      params.require(:subtask).permit(:todo)
    end
end
