# frozen_string_literal: true

class TasksController < ApplicationController
  before_action :authenticate_user!
  respond_to :js
  responders :flash

  def create
    @task = current_user.tasks.new(task_params)
    if @task.save
      @subtask = @task.subtasks.build
      respond_with(@task)
    else
      flash[:error] = "Can not be blank."
    end
  end

  private
    def task_params
      params.require(:task).permit(:todo, :private)
    end
end
