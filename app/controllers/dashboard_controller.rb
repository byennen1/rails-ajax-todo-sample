# frozen_string_literal: true

class DashboardController < ApplicationController
  def index
    @task = Task.new
    @subtask = Subtask.new
    @tasks = Task.all.order(created_at: :desc)
  end
end
