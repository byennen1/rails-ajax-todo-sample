# frozen_string_literal: true

class Subtask < ApplicationRecord
  belongs_to :task

  validates :todo, presence: true
end
