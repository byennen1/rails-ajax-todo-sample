# frozen_string_literal: true

FactoryBot.define do
  factory :task do
    todo "MyString"
    private false
  end
end
