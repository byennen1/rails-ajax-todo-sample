# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    password "changeme123"
    password_confirmation "changeme123"
  end
end
