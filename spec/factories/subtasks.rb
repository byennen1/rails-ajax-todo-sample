# frozen_string_literal: true

FactoryBot.define do
  factory :subtask do
    todo "MyString"
    private false
  end
end
