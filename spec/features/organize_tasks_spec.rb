# frozen_string_literal: true

require "rails_helper"

feature "Organize Tasks" do
  background do
    user = FactoryBot.create(:user)
    # login_as(user, scope: :user)
    visit new_user_registration_path
    within("#new_user") do
      fill_in "user_email", with: user.email
      fill_in "user_password", with: "password1"
      fill_in "user_password_confirmation", with: "password1"
      click_button("Sign up")
    end
  end

  scenario "Add new Task" do
    visit root_path
    fill_in "task_todo", with: "Be awesome!"
    click_button("Create Task")
    expect(page).to have_content("Be awesome!")
  end
end
