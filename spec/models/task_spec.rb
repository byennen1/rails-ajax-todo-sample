# frozen_string_literal: true

require "rails_helper"

RSpec.describe Task, type: :model do
  context "create validate user" do
    before(:each) {
      @user_attr = FactoryBot.attributes_for(:user)
      @user = User.create(@user_attr)
    }

    it "must have a valid string for a todo" do
      @user.tasks.create(todo: "task")
      expect(@user.tasks.count).to eq 1
    end

    it "it can be private" do
      @task = @user.tasks.create(todo: "private todo", private: true)
      expect(@task.private).to eq true
    end

    it "has many subtasks" do
      @task = @user.tasks.create(todo: "task")
      3.times do
        @task.subtasks.create!(todo: "subtask")
      end
      expect(@task.subtasks.count).to eq 3
    end
  end
end
