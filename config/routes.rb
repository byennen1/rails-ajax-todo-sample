# frozen_string_literal: true

Rails.application.routes.draw do
  resources :tasks, only: :create do
    resources :subtasks, only: :create
  end
  devise_for :users
  root "dashboard#index"
end
